package org.tds.sgh.system;

import java.util.GregorianCalendar;
import java.util.Set;

import javax.swing.event.InternalFrameListener;

import org.tds.sgh.business.CadenaHotelera;
import org.tds.sgh.business.Cliente;
import org.tds.sgh.business.Habitacion;
import org.tds.sgh.business.Hotel;
import org.tds.sgh.business.Huesped;
import org.tds.sgh.business.Reserva;
import org.tds.sgh.business.TipoHabitacion;
import org.tds.sgh.dtos.ClienteDTO;
import org.tds.sgh.dtos.DTO;
import org.tds.sgh.dtos.HabitacionDTO;
import org.tds.sgh.dtos.HotelDTO;
import org.tds.sgh.dtos.ReservaDTO;
import org.tds.sgh.dtos.TipoHabitacionDTO;
import org.tds.sgh.infrastructure.Infrastructure;


public abstract class BaseController implements IHacerReservaController, ICancelarReservaController, ITomarReservaController, IModificarReservaController, ICadenaController
{
	private CadenaHotelera cadenaHotelera;
	
	private final DTO DTO = org.tds.sgh.dtos.DTO.getInstance();
	
	private Cliente cliente;
	
	private Reserva reserva; 

	
	//Constructor
	// --------------------------------------------------------------------------------------------
	
	public BaseController(CadenaHotelera cadenaHotelera)
	{
		this.cadenaHotelera = cadenaHotelera;
	}
	
	//implementaciones
	// --------------------------------------------------------------------------------------------
	@Override
	public Set<ClienteDTO> buscarCliente(String patronNombreCliente) {
		return DTO.mapClientes(cadenaHotelera.buscarClientes(patronNombreCliente));
	}

	@Override
	public ClienteDTO seleccionarCliente(String rut) throws Exception {
		cliente=cadenaHotelera.buscarCliente(rut);
		return DTO.map(cliente);
	}

	@Override
	public Set<ReservaDTO> buscarReservasDelCliente() throws Exception {		
			return DTO.mapReservas(cliente.buscarReservas());
	}

	@Override
	public ReservaDTO seleccionarReserva(long codigoReserva) throws Exception {
		reserva=cadenaHotelera.seleccionarReserva(codigoReserva);
		this.cliente = reserva.getCliente();
		
		if (cliente == null) {
			throw new Exception("No hay cliente seleccionado");
		}
		
		if (!cliente.buscarReservas().contains(reserva))
			throw new Exception("La reserva no pertenece al cliente seleccionado");
		
		return DTO.map(reserva);
	}

	@Override
	public ClienteDTO registrarCliente(String rut, String nombre, String direccion, String telefono, String mail)
			throws Exception {
		cliente=cadenaHotelera.agregarCliente(rut, nombre, direccion, telefono, mail);
		return DTO.map(cliente);
	}

	@Override
	public ReservaDTO modificarReserva(String nombreHotel, String nombreTipoHabitacion, GregorianCalendar fechaInicio,
			GregorianCalendar fechaFin, boolean modificablePorHuesped) throws Exception {
		if (!reserva.isModificablePorHuesped())
			throw new Exception("La reserva no pertenece al cliente seleccionado");
		
			Hotel hotel= cadenaHotelera.buscarHotel(nombreHotel);
			TipoHabitacion tipoHabitacion= cadenaHotelera.buscarTipoHabitacion(nombreTipoHabitacion);
			reserva=reserva.modificarReserva(hotel, tipoHabitacion, fechaInicio, fechaFin, modificablePorHuesped);
			enviarMail(cliente.getMail(), "", "");
			return DTO.map(reserva);

			
	}

	@Override
	public Set<ReservaDTO> buscarReservasPendientes(String nombreHotel) throws Exception {
		Hotel hotel = cadenaHotelera.buscarHotel(nombreHotel);
		return DTO.mapReservas(hotel.buscarReservasPendientes());
	}

	@Override
	public ReservaDTO registrarHuesped(String nombre, String documento) throws Exception {
		reserva=reserva.registrarHuesped(nombre, documento);
		return DTO.map(reserva);
	}

	@Override
	public ReservaDTO tomarReserva() throws Exception {
		reserva=reserva.tomarReserva();
		cliente= reserva.getCliente();
		//porque tengo q sacar el cliente de la reserva y no de la variable global
		enviarMail(reserva.getCliente().getMail(), "", "");
		iniciarEstadia(DTO.map(reserva));
		return DTO.map(reserva);
	}

	@Override
	public ReservaDTO cancelarReservaDelCliente() throws Exception {
		//no se si hay q guardar reserva cancelada
		Reserva reservaCancelada= reserva.cancelarReserva();
		enviarMail(cliente.getMail(), "", "");
		return DTO.map(reservaCancelada);
	}

	@Override
	public boolean confirmarDisponibilidad(String nombreHotel, String nombreTipoHabitacion,
			GregorianCalendar fechaInicio, GregorianCalendar fechaFin) throws Exception {
		Hotel hotel= cadenaHotelera.buscarHotel(nombreHotel);
		TipoHabitacion tipoHabitacion= cadenaHotelera.buscarTipoHabitacion(nombreTipoHabitacion);
		return hotel.confirmarDiponibilidad(tipoHabitacion, fechaInicio, fechaFin);
	}

	@Override
	public ReservaDTO registrarReserva(String nombreHotel, String nombreTipoHabitacion, GregorianCalendar fechaInicio,
			GregorianCalendar fechaFin, boolean modificablePorHuesped) throws Exception {
		reserva= cadenaHotelera.registrarReserva(nombreHotel, nombreTipoHabitacion, fechaInicio, fechaFin, modificablePorHuesped, this.cliente);
		cliente.getReservas().put(reserva.getCodigo(), reserva);
		enviarMail(cliente.getMail(), "", "");
		return DTO.map(reserva);
	}

	@Override
	public Set<HotelDTO> sugerirAlternativas(String pais, String nombreTipoHabitacion, GregorianCalendar fechaInicio,
			GregorianCalendar fechaFin) throws Exception {
		return DTO.mapHoteles(cadenaHotelera.sugerirAlternativas(pais, nombreTipoHabitacion, fechaInicio, fechaFin));
	}


	public void enviarMail(String destinatario, String asunto, String mensaje) {
		Infrastructure.getInstance().getSistemaMensajeria().enviarMail(destinatario, asunto, mensaje);
		
	}
	
	public void iniciarEstadia(ReservaDTO reserva) {
		Infrastructure.getInstance().getSistemaFacturacion().iniciarEstadia(reserva);
	}
	
	//implementacion cadena controller
	
	@Override
	public ClienteDTO agregarCliente(
		String rut,
		String nombre,
		String direccion,
		String telefono,
		String mail) throws Exception
	{
		cliente = this.cadenaHotelera.agregarCliente(rut, nombre, direccion, telefono, mail);
		
		return DTO.map(cliente);
	}
	
	

	@Override
	public HabitacionDTO agregarHabitacion(
		String nombreHotel,
		String nombreTipoHabitacion,
		String nombre) throws Exception
	{
		Hotel hotel = this.cadenaHotelera.buscarHotel(nombreHotel);
		
		TipoHabitacion tipoHabitacion = this.cadenaHotelera.buscarTipoHabitacion(nombreTipoHabitacion);
		
		Habitacion habitacion = hotel.agregarHabitacion(tipoHabitacion, nombre);
		
		return DTO.map(hotel, habitacion);
	}
	
	@Override
	public HotelDTO agregarHotel(String nombre, String pais) throws Exception
	{
		Hotel hotel = this.cadenaHotelera.agregarHotel(nombre, pais);
		
		return DTO.map(hotel);
	}
	
	@Override
	public TipoHabitacionDTO agregarTipoHabitacion(String nombre) throws Exception
	{
		TipoHabitacion tipoHabitacion = this.cadenaHotelera.agregarTipoHabitacion(nombre);
		
		return DTO.map(tipoHabitacion);
	}
	
	@Override
	public Set<ClienteDTO> getClientes()
	{
		return DTO.mapClientes(cadenaHotelera.listarClientes());
	}
	
	@Override
	public Set<HabitacionDTO> getHabitaciones(String nombreHotel) throws Exception
	{
		Hotel hotel = cadenaHotelera.buscarHotel(nombreHotel);
		
		return DTO.mapHabitaciones(hotel, hotel.listarHabitaciones());
	}
	
	@Override
	public Set<HotelDTO> getHoteles()
	{
		return DTO.mapHoteles(cadenaHotelera.listarHoteles());
	}
	
	@Override
	public Set<TipoHabitacionDTO> getTiposHabitacion()
	{
		return DTO.mapTiposHabitacion(cadenaHotelera.listarTiposHabitacion());
	}
	
	
}
