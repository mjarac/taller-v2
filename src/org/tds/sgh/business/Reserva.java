package org.tds.sgh.business;
import java.security.SecureRandom;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

import org.tds.sgh.infrastructure.ICalendario;
import org.tds.sgh.infrastructure.Infrastructure;

@Entity
public class Reserva
{
	private long id;
	
	private static ICalendario cal;
	private Long codigo;
	private GregorianCalendar fechaInicio;
	private GregorianCalendar fechaFin;
	private boolean modificablePorHuesped;
	private EstadoReserva estado;
	private Habitacion habitacion;
	private Hotel hotel;
	private List<Huesped> huespedes;
	private Cliente cliente;
	private TipoHabitacion tipoHabitacion;
	
	//Constructor
	// --------------------------------------------------------------------------------------------
	
	
	public Reserva(GregorianCalendar fechaInicio, GregorianCalendar fechaFin,
			boolean modificablePorHuesped, EstadoReserva estado, Habitacion habitacion, Hotel hotel,
			List<Huesped> huespedes, Cliente cliente, TipoHabitacion tipoHabitacion) {
		super();
		SecureRandom ran = new SecureRandom();
		this.codigo = Math.abs(ran.nextLong());//System.currentTimeMillis();
		this.fechaInicio = fechaInicio;
		this.fechaFin = fechaFin;
		this.modificablePorHuesped = modificablePorHuesped;
		this.estado = estado;
		this.habitacion = habitacion;
		this.hotel = hotel;
		this.huespedes = new ArrayList<Huesped>();
		this.cliente = cliente;
		this.tipoHabitacion = tipoHabitacion;
	}
	
	public Reserva() {
		super();
	}

	//Metodos propios
	// --------------------------------------------------------------------------------------------

	public boolean hayConflicto(TipoHabitacion tipoHabitacion, GregorianCalendar fechaInicio, GregorianCalendar fechaFin) {
		System.out.println("Tipo hab-->"+tipoHabitacion.getNombre());
		System.out.println("Res Tipo hab-->"+this.tipoHabitacion.getNombre());
		System.out.println("Fecha ini-->"+fechaInicio.getTime());
		System.out.println("fecha  fin-->"+fechaFin.getTime());
		System.out.println("Res fecha ini-->"+this.fechaInicio.getTime());
		System.out.println("Res fecha fin-->"+this.fechaFin.getTime());
//		if (tipoHabitacion.getNombre().equals(this.tipoHabitacion.getNombre())) {
//			
//			if (cal.esMismoDia(fechaInicio, this.fechaFin)) {
//				System.out.println("NO HAY CONFLICTO");
//				return false;
//			}
//			
//			if ((cal.esMismoDia(fechaInicio, this.fechaInicio) || cal.esMismoDia(fechaInicio, this.fechaFin)) || (cal.esPosterior(fechaInicio, this.fechaInicio) || cal.esAnterior(fechaInicio, this.fechaFin)) ) {
//				System.out.println("HAY CONFLICTO");
//				return true;
//			}
//						
//			if ( (cal.esMismoDia(fechaFin, this.fechaFin) || cal.esMismoDia(fechaFin, this.fechaInicio)) ||  (cal.esPosterior(fechaFin, this.fechaInicio) || cal.esAnterior(fechaFin,this.fechaFin))) {
//				System.out.println("HAY CONFLICTO");
//				return true;
//			}
//			
//			
//			
//		}
//		System.out.println("NO HAY CONFLICTO");
//		return false;
		
		
		if (this.getEstado() != EstadoReserva.Pendiente && this.estado != EstadoReserva.Tomada)
			return false;

		if (!this.tipoHabitacion.getNombre().equals(tipoHabitacion.getNombre()))
			return false;

		if (cal.esMismoDia(fechaInicio, this.getFechaInicio()))
			return true;

		if (cal.esAnterior(this.getFechaInicio(), fechaInicio) && cal.esPosterior(this.getFechaFin(), fechaInicio))
			return true;

		if (cal.esMismoDia(fechaFin, this.getFechaFin()))
			return true;

		if (cal.esAnterior(this.getFechaInicio(), fechaFin) && cal.esPosterior(this.getFechaFin(), fechaFin))
			return true;

		if (cal.esAnterior(fechaInicio, this.getFechaInicio()) && cal.esPosterior(fechaFin, this.getFechaInicio()))
			return true;

		return false;
	}
	
	public Reserva modificarReserva(Hotel hotel,TipoHabitacion tipoHabitacion, GregorianCalendar fechaInicio, GregorianCalendar fechaFin, boolean modificablePorHuesped) {
		this.hotel.getReservas().remove(this.getCodigo());
		this.setHotel(hotel);
		this.setTipoHabitacion(tipoHabitacion);
		this.setFechaInicio(fechaInicio);
		this.setFechaFin(fechaFin);
		this.setModificablePorHuesped(modificablePorHuesped);
		this.hotel.getReservas().put(this.getCodigo(), this);
		return this;
	}
	
	public Reserva cancelarReserva() {
		this.getHotel().getReservas().remove(this.getCodigo());
		this.setEstado(EstadoReserva.Cancelada);		
		this.hotel.getReservas().put(this.getCodigo(), this);
		this.cliente.getReservas().remove(this.getCodigo());
		return this;
		
	}
	
	public Reserva registrarHuesped(String nombre, String documento) {
		Huesped huesped=new Huesped(nombre, documento);
		this.huespedes.add(huesped);
		return this;		
	}	
	
	public Reserva tomarReserva() {
		this.setEstado(EstadoReserva.Tomada);
		this.setHabitacion(this.getHotel().obtenerHabitacion(this.tipoHabitacion));
		this.getHotel().getReservas().remove(this.codigo);
		this.getHotel().getReservas().put(this.codigo, this);
		return this;
	}
	
	//Getters y Setters
	// --------------------------------------------------------------------------------------------
	

	public GregorianCalendar getFechaInicio() {
		return fechaInicio;
	}

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public void setFechaInicio(GregorianCalendar fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public GregorianCalendar getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(GregorianCalendar fechaFin) {
		this.fechaFin = fechaFin;
	}

	public boolean isModificablePorHuesped() {
		return modificablePorHuesped;
	}

	public void setModificablePorHuesped(boolean modificablePorHuesped) {
		this.modificablePorHuesped = modificablePorHuesped;
	}

	public EstadoReserva getEstado() {
		return estado;
	}

	public void setEstado(EstadoReserva estado) {
		this.estado = estado;
	}

	@OneToOne(cascade=CascadeType.ALL)
	public Habitacion getHabitacion() {
		return habitacion;
	}

	public void setHabitacion(Habitacion habitacion) {
		this.habitacion = habitacion;
	}

	@OneToOne(cascade=CascadeType.ALL)
	public Hotel getHotel() {
		return hotel;
	}

	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}

	@OneToMany(cascade=CascadeType.ALL)
	@MapKey(name="nombre")
	public List<Huesped> getHuespedes() {
		return huespedes;
	}

	public void setHuespedes(List<Huesped> huespedes) {
		this.huespedes = huespedes;
	}

	@OneToOne(cascade=CascadeType.ALL)
	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	@OneToOne(cascade=CascadeType.ALL)
	public TipoHabitacion getTipoHabitacion() {
		return tipoHabitacion;
	}

	public void setTipoHabitacion(TipoHabitacion tipoHabitacion) {
		this.tipoHabitacion = tipoHabitacion;
	}
	
	static {
		cal = Infrastructure.getInstance().getCalendario();
	}

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	// --------------------------------------------------------------------------------------------
	
	
	
}
