package org.tds.sgh.business;
import javax.persistence.*;

@Entity
public class Huesped
{
	private long id;
	// --------------------------------------------------------------------------------------------
	
	private String nombre;
	private String documento;
	
	//Constructor
	// --------------------------------------------------------------------------------------------
	
	public Huesped(String nombre, String documento) {
		super();
		this.nombre = nombre;
		this.documento = documento;
	}
	
	//Getters y Setters
	// --------------------------------------------------------------------------------------------
	
	public String getNombre()
	{
		return this.nombre;
	}	

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	
	
	
}
