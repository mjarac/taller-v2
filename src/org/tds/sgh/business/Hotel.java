package org.tds.sgh.business;

import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.hibernate.loader.custom.Return;
import org.tds.sgh.infrastructure.Infrastructure;
import javax.persistence.*;

@Entity
public class Hotel
{
	private long id;
	// --------------------------------------------------------------------------------------------
	
	private Map<String, Habitacion> habitaciones;
	
	private String nombre;
	
	private String pais;
	
	private Map<Long, Reserva> reservas;
	
	//Constructor
	// --------------------------------------------------------------------------------------------
	
	public Hotel(String nombre, String pais)
	{
		this.habitaciones = new HashMap<String, Habitacion>();
		
		this.reservas = new HashMap<Long, Reserva>();
		
		this.nombre = nombre;
		
		this.pais = pais;
	}
	
	//Metodos implementados
	// --------------------------------------------------------------------------------------------
	
	public Habitacion agregarHabitacion(TipoHabitacion tipoHabitacion, String nombre) throws Exception
	{
		if (this.habitaciones.containsKey(nombre))
		{
			throw new Exception("El hotel ya tiene una habitación con el nombre indicado.");
		}
		
		Habitacion habitacion = new Habitacion(tipoHabitacion, nombre);
		
		this.habitaciones.put(habitacion.getNombre(), habitacion);
		
		return habitacion;
	}
	
	public Set<Habitacion> listarHabitaciones()
	{
		return new HashSet<Habitacion>(this.habitaciones.values());
	}
	
	//Metodos propios
	// --------------------------------------------------------------------------------------------
	public boolean confirmarDiponibilidad(TipoHabitacion tipoHabitacion, GregorianCalendar fechaInicio,GregorianCalendar fechaFin) throws Exception {
		if (Infrastructure.getInstance().getCalendario().esPasada(fechaInicio))
			throw new Exception("fecha pasada");
			
		if(Infrastructure.getInstance().getCalendario().esPosterior(fechaInicio, fechaFin))
			throw new Exception("fecha inicio es posterior a fecha de fin");
//		if (Infrastructure.getInstance().getCalendario().esPasada(fechaFin))
//		throw new Exception("fecha pasada");
		
		int cantidadHabitaciones=contarHabitaciones(tipoHabitacion);
		int cantidadReservasConflicto=contarReservas(tipoHabitacion, fechaInicio, fechaFin);
		if (cantidadReservasConflicto<cantidadHabitaciones)
			return true;
		return false;
	}
	
	private Integer contarHabitaciones(TipoHabitacion tipoHabitacion) {
		int contarHabitacion = 0;
		for (Map.Entry<String, Habitacion> entry : this.habitaciones.entrySet()) {
		    if (entry.getValue().coincideTipo(tipoHabitacion)) {
		    	contarHabitacion ++;
		    }
		}
		return contarHabitacion;
	}
	
	private Integer contarReservas(TipoHabitacion tipoHabitacion,GregorianCalendar fechaInicio,GregorianCalendar fechaFin ) {
		int contarReservas = 0;
		//Quizas recorrer solo reservas pendientes
		for (Map.Entry<Long, Reserva> entry : this.reservas.entrySet()) {			
		    if (entry.getValue().hayConflicto(tipoHabitacion, fechaInicio, fechaFin)) {
		    	contarReservas ++;
		    }
		}
		return contarReservas;
	}
	
	public Reserva crearReserva(TipoHabitacion tipoHabitacion, GregorianCalendar fechaInicio, GregorianCalendar fechaFin, boolean modificablePorHuesped, Cliente cliente) {
		Reserva reserva= new Reserva(fechaInicio, fechaFin, modificablePorHuesped, EstadoReserva.Pendiente, null, this, null, cliente, tipoHabitacion);
		this.reservas.put(reserva.getCodigo(), reserva);
		return reserva;
	}
	
	
	public Set<Reserva> buscarReservasPendientes(){
		Set<Reserva> reservasPendientes= new HashSet<Reserva>();
		for (Map.Entry<Long, Reserva> entry : this.reservas.entrySet()) {			
		    if (entry.getValue().getEstado().equals(EstadoReserva.Pendiente)) {
		    	reservasPendientes.add(entry.getValue());
		    }
		}
		return reservasPendientes;
	}
	
	public Reserva seleccionarReserva(Long codigo) throws Exception
	{		
		return this.reservas.get(codigo);
	}
	
	public Habitacion obtenerHabitacion(TipoHabitacion tipoHabitacion) {
		//Quizas deba obtener habitaciones q no esten en una reserva
		Habitacion habitacion = new Habitacion();
		for (Map.Entry<String, Habitacion> entry : this.habitaciones.entrySet()) {
		    if (entry.getValue().coincideTipo(tipoHabitacion)) {
		    	habitacion= entry.getValue();
		    }
		}
		
		return habitacion;
	}
	
	//Getters y Setters
	// --------------------------------------------------------------------------------------------
	
	public String getNombre()
	{
		return this.nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getPais()
	{
		return this.pais;
	}
	
	public void setPais(String pais) {
		this.pais = pais;
	}

	@OneToMany(cascade=CascadeType.ALL)
	@MapKey(name="nombre")
	public Map<String, Habitacion> getHabitaciones() {
		return habitaciones;
	}

	public void setHabitaciones(Map<String, Habitacion> habitaciones) {
		this.habitaciones = habitaciones;
	}

	@OneToMany(cascade=CascadeType.ALL)
	@MapKey(name="codigo")
	public Map<Long, Reserva> getReservas() {
		return reservas;
	}

	public void setReservas(Map<Long, Reserva> reservas) {
		this.reservas = reservas;
	}

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	

	

	
	
	
}
