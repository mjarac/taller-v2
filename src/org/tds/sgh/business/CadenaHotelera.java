package org.tds.sgh.business;

import java.util.GregorianCalendar;
import javax.persistence.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.tds.sgh.infrastructure.Infrastructure;

@Entity
public class CadenaHotelera
{
	
	private long id;
	// --------------------------------------------------------------------------------------------
	
	private Map<String, Cliente> clientes;
	
	private Map<String, Hotel> hoteles;
	
	private String nombre;
	
	private Map<String, TipoHabitacion> tiposHabitacion;
	
	//Constructor
	// --------------------------------------------------------------------------------------------
	
	public CadenaHotelera(String nombre)
	{
		this.clientes = new HashMap<String, Cliente>();
		
		this.hoteles = new HashMap<String, Hotel>();
		
		this.nombre = nombre;
		
		this.tiposHabitacion = new HashMap<String, TipoHabitacion>();
	}
	
	//Metodos Implementados
	// --------------------------------------------------------------------------------------------
	
	public Cliente agregarCliente(
		String rut,
		String nombre,
		String direccion,
		String telefono,
		String mail) throws Exception
	{
		if (this.clientes.containsKey(rut))
		{
			throw new Exception("Ya existe un cliente con el RUT indicado.");
		}
		
		Cliente cliente = new Cliente(rut, nombre, direccion, telefono, mail);
		
		this.clientes.put(cliente.getRut(), cliente);
		
		return cliente;
	}
	
	public Hotel agregarHotel(String nombre, String pais) throws Exception
	{
		if (this.hoteles.containsKey(nombre))
		{
			throw new Exception("Ya existe un hotel con el nombre indicado.");
		}
		
		Hotel hotel = new Hotel(nombre, pais);
		
		this.hoteles.put(hotel.getNombre(), hotel);
		
		return hotel;
	}
	
	public TipoHabitacion agregarTipoHabitacion(String nombre) throws Exception
	{
		if (this.tiposHabitacion.containsKey(nombre))
		{
			throw new Exception("Ya existe un tipo de habitación con el nombre indicado.");
		}
		
		TipoHabitacion tipoHabitacion = new TipoHabitacion(nombre);
		
		this.tiposHabitacion.put(tipoHabitacion.getNombre(), tipoHabitacion);
		
		return tipoHabitacion;
	}
	
	public Cliente buscarCliente(String rut) throws Exception
	{
		Cliente cliente = this.clientes.get(rut);
		
		if (cliente == null)
		{
			throw new Exception("No existe un cliente con el nombre indicado.");
		}
		
		return cliente;
	}
	
	public Set<Cliente> buscarClientes(String patronNombreCliente)
	{
		if (patronNombreCliente == null)
			throw new NullPointerException("patronNombreCliente no debe ser null");
		
		Set<Cliente> clientesEncontrados = new HashSet<Cliente>();
		
		for (Cliente cliente : this.clientes.values())
		{
			if (cliente.coincideElNombre(patronNombreCliente))
			{
				clientesEncontrados.add(cliente);
			}
		}
		
		return clientesEncontrados;
	}
	
	public Hotel buscarHotel(String nombre) throws Exception
	{
		Hotel hotel = this.hoteles.get(nombre);
		
		if (hotel == null)
		{
			throw new Exception("No existe un hotel con el nombre indicado.");
		}
		
		return hotel;
	}
	
	public TipoHabitacion buscarTipoHabitacion(String nombre) throws Exception
	{
		TipoHabitacion tipoHabitacion = this.tiposHabitacion.get(nombre);
		
		if (tipoHabitacion == null)
		{
			throw new Exception("No existe un tipo de habitación con el nombre indicado.");
		}
		
		return tipoHabitacion;
	}
	
	public Set<Cliente> listarClientes()
	{
		return new HashSet<Cliente>(this.clientes.values());
	}
	
	public Set<Hotel> listarHoteles()
	{
		return new HashSet<Hotel>(this.hoteles.values());
	}
	
	public Set<TipoHabitacion> listarTiposHabitacion()
	{
		return new HashSet<TipoHabitacion>(this.tiposHabitacion.values());
	}
	
	//Metodos propios
	// --------------------------------------------------------------------------------------------
	
	public Set<Hotel> sugerirAlternativas(String pais, String nombreTipoHabitacion, GregorianCalendar fechaInicio, GregorianCalendar fechaFin) throws Exception{
		
		if (Infrastructure.getInstance().getCalendario().esPasada(fechaInicio))
			throw new Exception("fecha pasada");
			
		if(Infrastructure.getInstance().getCalendario().esPosterior(fechaInicio, fechaFin))
			throw new Exception("fecha inicio es posterior a fecha de fin");
		
		Set<Hotel> hoteles= new HashSet<Hotel>();
		TipoHabitacion tipoHabitacion=this.buscarTipoHabitacion(nombreTipoHabitacion); 
		for (Map.Entry<String, Hotel> entry : this.hoteles.entrySet()) {			
		    if (entry.getValue().getPais().equals(pais))
		    	if (entry.getValue().confirmarDiponibilidad(tipoHabitacion, fechaInicio, fechaFin)) {
		    		hoteles.add(entry.getValue());
		    }
		}
		return hoteles;
	}
	
	public Reserva registrarReserva(String nombreHotel, String nombreTipoHabitacion,GregorianCalendar fechaInicio, GregorianCalendar fechaFin, boolean modificablePorHuesped, Cliente cliente) throws Exception {
		Hotel hotel= this.buscarHotel(nombreHotel);
		TipoHabitacion tipoHabitacion= this.buscarTipoHabitacion(nombreTipoHabitacion);
		Reserva resTemporal=hotel.crearReserva(tipoHabitacion, fechaInicio, fechaFin, modificablePorHuesped, cliente); 
		return resTemporal;
	}
	
	public Reserva seleccionarReserva(Long codigo) throws Exception {
		Reserva reserva;
		for (Map.Entry<String, Hotel> entry : this.hoteles.entrySet()) {			
		    reserva=entry.getValue().seleccionarReserva(codigo);
		    if (reserva != null)
		    		return reserva;
		}
		throw new Exception ("reserva no existe");
	}
	
	
//	public Reserva seleccionarReserva(long codigoReserva) throws Exception {
//		Reserva reserva;
//			for (Hotel hotel : this.hoteles.values()) {
//				reserva = hotel.seleccionarReserva(codigoReserva);
//				
//				if (reserva != null)
//					return reserva;
//			}
//
//			throw new Exception ("reserva no existe");
//	}
	
	
	//Getters y Setters
	// --------------------------------------------------------------------------------------------
	
	public String getNombre()
	{
		return this.nombre;
	}

	@OneToMany(cascade=CascadeType.ALL)
	@MapKey(name="rut")
	public Map<String, Cliente> getClientes() {
		return clientes;
	}

	public void setClientes(Map<String, Cliente> clientes) {
		this.clientes = clientes;
	}

	@OneToMany(cascade=CascadeType.ALL)
	@MapKey(name="nombre")
	public Map<String, Hotel> getHoteles() {
		return hoteles;
	}

	public void setHoteles(Map<String, Hotel> hoteles) {
		this.hoteles = hoteles;
	}

	@OneToMany(cascade=CascadeType.ALL)
	@MapKey(name="nombre")
	public Map<String, TipoHabitacion> getTiposHabitacion() {
		return tiposHabitacion;
	}

	public void setTiposHabitacion(Map<String, TipoHabitacion> tiposHabitacion) {
		this.tiposHabitacion = tiposHabitacion;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	
	
	
	
}
