package org.tds.sgh.business;

import java.util.HashMap;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.tds.sgh.infrastructure.ICalendario;
import org.tds.sgh.infrastructure.Infrastructure;
import javax.persistence.*;

@Entity
public class Cliente
{
	private long id;
	// --------------------------------------------------------------------------------------------
	
	private String direccion;
	
	private String mail;
	
	private String nombre;
	
	private String rut;
	
	private String telefono;
	
	private Map<Long, Reserva> reservas;
	
	private static ICalendario cal;
	
	//Constructor
	// --------------------------------------------------------------------------------------------
	
	public Cliente(String rut, String nombre, String direccion, String telefono, String mail)
	{
		this.direccion = direccion;
		
		this.mail = mail;
		
		this.nombre = nombre;
		
		this.rut = rut;
		
		this.telefono = telefono;
		
		this.reservas = new HashMap<Long, Reserva>();
	}
	
	//Metodos implementados
	// --------------------------------------------------------------------------------------------
	
	public boolean coincideElNombre(String patronNombreCliente)
	{
		return this.nombre.matches(patronNombreCliente);
	}
	
	//Metodos propios
	// --------------------------------------------------------------------------------------------
	
	@Transient
	public Set<Reserva> buscarReservas(){
		Set<Reserva> reservasEncontrados = new HashSet<Reserva>();
		for (Map.Entry<Long, Reserva> entry : this.reservas.entrySet()) {	
			System.out.println("estado: "+entry.getValue().getEstado().equals(EstadoReserva.Pendiente));
			System.out.println("hoy: "+cal.esHoy(entry.getValue().getFechaInicio()));
			System.out.println("futura: "+cal.esFutura(entry.getValue().getFechaInicio()));
			if(entry.getValue().getEstado().equals(EstadoReserva.Pendiente) && 
					(cal.esHoy(entry.getValue().getFechaInicio()) || cal.esFutura(entry.getValue().getFechaInicio())))
				reservasEncontrados.add(entry.getValue());
		    /*if (entry.getValue().getEstado().equals(EstadoReserva.Pendiente) ||
		    	(cal.esHoy(entry.getValue().getFechaInicio()) || cal.esFutura(entry.getValue().getFechaInicio()))) {
		    	reservasEncontrados.add(entry.getValue());
		    }*/
		}
		return reservasEncontrados;
	}
	
	//Getters y Setters
	// --------------------------------------------------------------------------------------------
	
	public String getDireccion()
	{
		return this.direccion;
	}
	
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	
	public String getMail()
	{
		return this.mail;
	}
	
	public void setMail(String mail) {
		this.mail = mail;
	}
	
	public String getNombre()
	{
		return this.nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getRut()
	{
		return this.rut;
	}
	
	public void setRut(String rut) {
		this.rut = rut;
	}
	
	public String getTelefono()
	{
		return this.telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	@OneToMany(cascade=CascadeType.ALL)
	@MapKey(name="codigo")
	public Map<Long, Reserva> getReservas() {
		return reservas;
	}

	public void setReservas(Map<Long, Reserva> reservas) {
		this.reservas = reservas;
	}

	static {
		cal = Infrastructure.getInstance().getCalendario();
	}

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	
	
}
