package org.tds.sgh.business;
import javax.persistence.*;

@Entity
public class Habitacion
{
	private long id;
	// --------------------------------------------------------------------------------------------
	
	private String nombre;
	
	private TipoHabitacion tipoHabitacion;
	
	//Constructor
	// --------------------------------------------------------------------------------------------
	
	public Habitacion(TipoHabitacion tipoHabitacion, String nombre)
	{
		this.nombre = nombre;
		
		this.tipoHabitacion = tipoHabitacion;
	}
	
	public Habitacion() {
		super();
	}

	
	//Metodos Propios
	// --------------------------------------------------------------------------------------------
	
	public boolean coincideTipo(TipoHabitacion tipoHabitacion) {
		if (this.tipoHabitacion.getNombre().equals(tipoHabitacion.getNombre())) {
			return true;
		}
		return false;
	}
	
	//Getters y Setters
	// --------------------------------------------------------------------------------------------
	
	public String getNombre()
	{
		return this.nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@OneToOne(cascade=CascadeType.ALL)
	public TipoHabitacion getTipoHabitacion()
	{
		return this.tipoHabitacion;
	}
	
	public void setTipoHabitacion(TipoHabitacion tipoHabitacion) {
		this.tipoHabitacion = tipoHabitacion;
	}

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	
}
